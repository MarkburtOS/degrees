import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:degrees/models/AppHomeScreen.dart';
import 'package:degrees/pages/Homepage.dart';
final routes = {

  '/': (context) => FitnessAppHomeScreen(),
  '/Home': (context) => Homepage(),
  //需要带参数跳转
  // '/searches': (context, {arguments}) => (arguments: arguments),
};

///固定写法
// ignore: missing_return, top_level_function_literal_block
var onGenerateRoute = (RouteSettings settings) {
//统一处理
  final String name = settings.name;
  final Function pageContentBuilder = routes[name];
  if (pageContentBuilder != null) {
    if (settings.arguments != null) {
      final Route route = MaterialPageRoute(
          builder: (context) =>
              pageContentBuilder(context, arguments: settings.arguments));
      return route;
    } else {
      final Route route =
          MaterialPageRoute(builder: (context) => pageContentBuilder(context));
      return route;
    }
  }
};
